rootProject.name = "gitlab-jetbrains-plugin"

pluginManagement {
  repositories {
    mavenCentral()
    gradlePluginPortal()
  }
}

dependencyResolutionManagement {
  versionCatalogs {
    create("libs") {
      // Plugins
      val apollo3 = version("apollo3", "3.8.2")
      val detekt = version("detekt", "1.23.4")
      val kotlin = version("kotlin", "1.9.10")

      plugin("apollographql-apollo3", "com.apollographql.apollo3").versionRef(apollo3)
      plugin("github-gmazzo-buildconfig", "com.github.gmazzo.buildconfig").version("5.3.2")
      plugin("github-gradle-node", "com.github.node-gradle.node").version("7.0.1")
      plugin("gitlab-arturbosch-detekt", "io.gitlab.arturbosch.detekt").versionRef(detekt)
      plugin("jetbrains-changelog", "org.jetbrains.changelog").version("2.2.0")
      plugin("jetbrains-intellij", "org.jetbrains.intellij").version("1.16.1")
      plugin("jetbrains-kotlin-jvm", "org.jetbrains.kotlin.jvm").versionRef(kotlin)
      plugin("jetbrains-kotlin-serialization", "org.jetbrains.kotlin.plugin.serialization").versionRef(kotlin)

      // Dependencies
      val ktor = version("ktor", "2.3.5")

      library("apollo.graphql.runtime", "com.apollographql.apollo3", "apollo-runtime").versionRef(apollo3)
      library("commons.net", "commons-net", "commons-net").version("3.9.0")
      library("jackson.databind", "com.fasterxml.jackson.core", "jackson-databind").version("2.15.3")
      library("kotlinx-datetime", "org.jetbrains.kotlinx", "kotlinx-datetime").version("0.5.0")
      library("ktor.client.auth", "io.ktor", "ktor-client-auth").versionRef(ktor)
      library("ktor.client.contentNegotiation", "io.ktor", "ktor-client-content-negotiation").versionRef(ktor)
      library("ktor.client.logging", "io.ktor", "ktor-client-logging").versionRef(ktor)
      library("ktor.client.okhttp", "io.ktor", "ktor-client-okhttp").versionRef(ktor)
      library("ktor.serialization.gson", "io.ktor", "ktor-serialization-gson").versionRef(ktor)
      library("ktor.serialization.kotlinxJson", "io.ktor", "ktor-serialization-kotlinx-json").versionRef(ktor)
      library("logback.classic", "ch.qos.logback", "logback-classic").version("1.4.14")
      library("snowplow.java.tracker", "com.snowplowanalytics", "snowplow-java-tracker").version("1.0.0")

      // Test dependencies
      val junit = version("junit", "5.9.3")
      val kotest = version("kotest", "5.7.2")
      val remoterobot = version("remoterobot", "0.11.20")

      library("apollo.graphql.mockserver", "com.apollographql.apollo3", "apollo-mockserver").versionRef(apollo3)
      library("apollo.graphql.testingSupport", "com.apollographql.apollo3", "apollo-testing-support").versionRef(apollo3)
      library("detekt.formatting", "io.gitlab.arturbosch.detekt", "detekt-formatting").versionRef(detekt)
      library("junit.jupiter", "org.junit.jupiter", "junit-jupiter").versionRef(junit)
      library("junit.jupiter.api", "org.junit.jupiter", "junit-jupiter-api").versionRef(junit)
      library("junit.jupiter.engine", "org.junit.jupiter", "junit-jupiter-engine").versionRef(junit)
      library("junit.jupiter.params", "org.junit.jupiter", "junit-jupiter-params").versionRef(junit)
      library("junit.platform.launcher", "org.junit.platform", "junit-platform-launcher").version("1.9.3")
      library("junit.platform.suite.engine", "org.junit.platform", "junit-platform-suite-engine").version("1.10.1")
      library("kotest.assertions.core", "io.kotest", "kotest-assertions-core").versionRef(kotest)
      library("kotest.framework.datatest", "io.kotest", "kotest-framework-datatest").versionRef(kotest)
      library("kotest.runner.junit5", "io.kotest", "kotest-runner-junit5").versionRef(kotest)
      library("kotlin.test.junit", "org.jetbrains.kotlin", "kotlin-test-junit").versionRef(kotlin)
      library("kotlinx.coroutines.test", "org.jetbrains.kotlinx", "kotlinx-coroutines-test").version("1.7.1")
      library("ktor.client.mock", "io.ktor", "ktor-client-mock").versionRef(ktor)
      library("mockk", "io.mockk", "mockk").version("1.13.5")
      library("mockserver.junit.jupiter", "org.mock-server", "mockserver-junit-jupiter-no-dependencies").version("5.14.0")
      library("okhttp3.logging.interceptor", "com.squareup.okhttp3", "logging-interceptor").version("4.11.0")
      library("remote.fixtures", "com.intellij.remoterobot", "remote-fixtures").versionRef(remoterobot)
      library("remote.robot", "com.intellij.remoterobot", "remote-robot").versionRef(remoterobot)
      library("video.recorder.junit5", "com.automation-remarks", "video-recorder-junit5").version("2.0")
    }
  }
}
