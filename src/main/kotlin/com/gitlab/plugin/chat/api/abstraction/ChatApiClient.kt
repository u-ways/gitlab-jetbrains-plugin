package com.gitlab.plugin.chat.api.abstraction

import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage

interface ChatApiClient {
  suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String
  ): AiActionResponse

  suspend fun subscribeToUpdates(
    subscriptionId: String,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  )
}
