package com.gitlab.plugin.activities

import com.gitlab.plugin.codesuggestions.license.LicenseStatus
import com.gitlab.plugin.services.GitLabProjectService
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.util.gitlabStatusVersionUnsupported
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity

internal class GitLabProjectStartup : ProjectActivity {
  private val logger = logger<GitLabProjectStartup>()

  override suspend fun execute(project: Project) {
    val projectService = GitLabProjectService(project)
    val context = ProjectContextService.init(project)

    context.codeSuggestionsLicense.refresh()
    val token = TokenUtil.getToken()

    val notification = when {
      !context.codeSuggestionsLicense.isLicensed -> {
        LicenseStatus.licenseMissingNotification()
      }

      context.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
        gitlabStatusVersionUnsupported()
        GitLabServerService.versionUnsupportedNotification()
      }

      !token.isNullOrEmpty() -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "GitLab Duo Code Suggestions is ready to use with this project."
        )
      }

      else -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "You need to configure your GitLab credentials first.",
          listOf(NotificationAction.settings(project))
        )
      }
    }

    GitLabNotificationManager().sendNotification(notification)

    projectService.withRepository { repository ->
      if (repository != null) logger.info("Found Git repository for project")
    }
  }
}
