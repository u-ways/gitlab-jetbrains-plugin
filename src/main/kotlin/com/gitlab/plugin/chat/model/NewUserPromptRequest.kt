package com.gitlab.plugin.chat.model

data class NewUserPromptRequest(
  val content: String,
  val type: ChatRecord.Type,
  val context: ChatRecordContext? = null
) {
  constructor(content: String) : this(content, detectTypeViaContent(content))

  companion object {
    private fun detectTypeViaContent(
      content: String?
    ): ChatRecord.Type = if (content == "/reset") {
      ChatRecord.Type.NEW_CONVERSATION
    } else {
      ChatRecord.Type.GENERAL
    }
  }
}
