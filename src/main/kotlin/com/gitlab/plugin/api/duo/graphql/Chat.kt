package com.gitlab.plugin.api.duo.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.chat.exceptions.UnsupportedGitLabVersionException
import com.gitlab.plugin.graphql.ChatMutation
import com.gitlab.plugin.graphql.ChatQuery
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.type.AiChatInput
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class Chat(private val apolloClient: ApolloClient) {
  @Suppress("DEPRECATION")
  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  suspend fun getMessages(requestIds: List<String>?): ChatQuery.Messages? = try {
    val query = ChatQuery(Optional.present(requestIds))
    apolloClient.query(query)
      .execute()
      .dataAssertNoErrors
      .messages
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  @Suppress("DEPRECATION")
  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  suspend fun send(chatInput: AiChatInput, clientSubscriptionId: String): ChatMutation.Action? = try {
    val mutation = ChatMutation(chatInput, clientSubscriptionId)
    apolloClient.mutation(mutation)
      .execute()
      .dataAssertNoErrors
      .action
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  @Suppress("DEPRECATION")
  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  fun subscription(
    clientSubscriptionId: String,
  ): Flow<ChatSubscription.Response?> = try {
    val subscription = ChatSubscription(clientSubscriptionId)
    apolloClient.subscription(subscription)
      .toFlow()
      .map { it.dataAssertNoErrors }
      .map { it.response }
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }
}
