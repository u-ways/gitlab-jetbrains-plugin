package com.gitlab.plugin.api.pat

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabResponseException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.collaboration.util.resolveRelative
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.net.URI
import java.nio.channels.UnresolvedAddressException

private const val DEFAULT_SERVER_URL = "https://gitlab.com"

class PatApi(private val token: String, host: String = DEFAULT_SERVER_URL, engine: HttpClientEngine? = null) {
  private val apiUrl = URI(host).resolveRelative("api/v4/")

  private val client = HttpClient(engine ?: proxiedEngine()) {
    expectSuccess = true

    defaultRequest {
      contentType(ContentType.Application.Json)
    }

    install(UserAgent) {
      agent = GitLabUtil.userAgent
    }

    install(HttpRequestRetry) {
      retryOnServerErrors(maxRetries = 3)
      exponentialDelay()
    }

    install(Auth) {
      bearer {
        loadTokens {
          BearerTokens(token, "")
        }
      }
    }

    install(ContentNegotiation) {
      json(Json { isLenient = true })
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        when (cause) {
          is ClientRequestException -> {
            val exceptionResponse = cause.response
            val exceptionResponseText = exceptionResponse.bodyAsText()

            when (exceptionResponse.status) {
              HttpStatusCode.Unauthorized -> throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
              else -> throw GitLabResponseException(exceptionResponse, exceptionResponseText)
            }
          }

          is UnresolvedAddressException -> throw GitLabOfflineException(request, cause)
          is ConnectTimeoutException -> throw GitLabOfflineException(request, cause)
          is HttpRequestTimeoutException -> throw GitLabOfflineException(request, cause)
          else -> throw cause
        }
      }
    }
  }

  suspend fun patInfo(): PatInfo {
    val url = apiUrl.resolveRelative("personal_access_tokens/self").toString()

    return client.get(url).body()
  }
}
