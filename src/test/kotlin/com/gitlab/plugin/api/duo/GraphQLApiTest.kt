package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.mockserver.MockResponse
import com.apollographql.apollo3.mockserver.MockServer
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.common.runBlocking
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk

class GraphQLApiTest : DescribeSpec({
  lateinit var graphqlApi: GraphQLApi
  lateinit var mockServer: MockServer
  val apolloClientFactory = mockk<ApolloClientFactory>()

  beforeEach {
    mockServer = MockServer()
    graphqlApi = GraphQLApi(apolloClientFactory)

    every { apolloClientFactory.create() } returns ApolloClient.Builder()
      .serverUrl(runBlocking { mockServer.url() })
      .build()
  }

  afterEach {
    runBlocking { mockServer.stop() }
    clearAllMocks()
  }

  describe("getCurrentUser") {
    @Suppress("DEPRECATION")
    it("returns a valid user") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "duoChatAvailable": true,
                  "duoCodeSuggestionsAvailable": true,
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldNotBeNull().shouldBeTrue()
      actual.duoCodeSuggestionsAvailable.shouldNotBeNull().shouldBeTrue()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }

    @Suppress("DEPRECATION")
    it("returns null for fields absent from old schema versions") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldBeNull()
      actual.duoCodeSuggestionsAvailable.shouldBeNull()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }
  }

  describe("server errors") {
    it("should throw for a 500 internal server error") {
      mockServer.enqueue(
        MockResponse.Builder().body("Internal server error").statusCode(500).build()
      )

      shouldThrow<GitLabGraphQLResponseException> {
        runBlocking { graphqlApi.getCurrentUser() }
      }
    }
  }
})
