package com.gitlab.plugin.services.chat

import com.gitlab.plugin.chat.ChatCommandInvoker
import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger

/**
 * Service for invoking commands within the chat plugin context.
 * This service maps chat commands to corresponding IntelliJ actions and executes them.
 */
@Service(Service.Level.PROJECT)
@Suppress("detekt:TooGenericExceptionCaught")
class ChatCommandInvokerService : ChatCommandInvoker {
  private val logger = logger<ChatCommandInvokerService>()

  companion object {
    private const val ACTION_PLACE = "ChatCommandInvokerService"
    private const val EXPLAIN_CODE_ACTION_ID = "com.gitlab.plugin.actions.chat.ExplainCodeChatAction"
    private const val GENERATE_TESTS_ACTION_ID = "com.gitlab.plugin.actions.chat.GenerateTestsChatAction"
    private const val REFACTOR_CODE_ACTION_ID = "com.gitlab.plugin.actions.chat.RefactorCodeChatAction"
    private val COMMAND_ACTION_MAP = hashMapOf(
      "/explain" to EXPLAIN_CODE_ACTION_ID,
      "/tests" to GENERATE_TESTS_ACTION_ID,
      "/refactor" to REFACTOR_CODE_ACTION_ID
    )
  }

  /**
   * Invokes an IntelliJ action based on a specific chat command.
   *
   * @param command The command string received from chat.
   */
  override fun invokeCommand(
    command: String
  ) {
    val actionId = COMMAND_ACTION_MAP[command] ?: run {
      logger.info("No action mapped for command: '$command'")
      return
    }

    val action = ActionManager.getInstance().getAction(actionId) ?: run {
      logger.info("Action with ID $actionId not found or is not AnAction")
      return
    }

    DataManager.getInstance().dataContextFromFocusAsync.onSuccess { dataContext ->
      val event = AnActionEvent.createFromDataContext(ACTION_PLACE, null, dataContext)
      action.update(event)
      if (event.presentation.isEnabled && event.presentation.isVisible) {
        try {
          action.actionPerformed(event)
        } catch (e: Exception) {
          logger.error("Error performing action $actionId", e)
        }
      } else {
        logger.info("Action $actionId is not enabled or visible under current context")
      }
    }
  }

  override fun isKnownCommand(command: String): Boolean = COMMAND_ACTION_MAP.containsKey(command)
}
