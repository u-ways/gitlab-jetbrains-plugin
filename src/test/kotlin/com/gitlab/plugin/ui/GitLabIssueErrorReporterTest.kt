package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.util.Consumer
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.ints.shouldBeLessThan
import io.kotest.matchers.string.shouldContain
import io.ktor.http.*
import io.mockk.*
import java.awt.Component

class GitLabIssueErrorReporterTest : DescribeSpec({
  val errorReporter = GitLabIssueErrorReporter()

  describe("submit") {
    mockkObject(GitLabUtil)
    mockkObject(GitLabBundle)
    mockkStatic(ApplicationInfo::getInstance)

    val event: IdeaLoggingEvent = mockk()
    val parentComponent: Component = mockk()
    val consumer: Consumer<SubmittedReportInfo> = mockk()

    val additionalInfo = "More details"
    val stackTrace = "This is a stack trace"
    val pluginVersion = "0.5.1"
    val applicationName = "Full application name"
    val applicationBuild = "Build string"

    beforeEach {
      every { GitLabUtil.browseUrl(any()) } just runs
      every { ApplicationInfo.getInstance().fullApplicationName } returns applicationName
      every { ApplicationInfo.getInstance().build.asString() } returns applicationBuild
      every { GitLabBundle.plugin()?.version } returns pluginVersion

      every { event.throwableText } returns stackTrace
      every { consumer.consume(any()) } just runs
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    it("opens a browser with the given URL") {
      errorReporter.submit(arrayOf(event), additionalInfo, parentComponent, consumer)

      verify(exactly = 1) {
        GitLabUtil.browseUrl(any())
      }
    }

    describe("issue URL") {
      it("has the correct path and params") {
        val urlSlot = slot<String>()
        every { GitLabUtil.browseUrl(capture(urlSlot)) } just runs

        errorReporter.submit(arrayOf(event), additionalInfo, parentComponent, consumer)

        urlSlot.captured.decodeURLPart().let {
          it shouldContain "gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new"
          it shouldContain "issuable_template=Bug"
          it shouldContain "issue[description]=## Debug information"
          it shouldContain stackTrace
          it shouldContain pluginVersion
          it shouldContain additionalInfo
          it shouldContain applicationName
          it shouldContain applicationBuild
        }
      }
    }

    context("when the issue URL is over header character limit") {
      val longStackTrace =
        "at com.gitlab.plugin.ui.GitLabIssueErrorReporterTest\$1\$1\$7\$2.invokeSuspend(GitLabIssueErrorReporterTest.kt:88)".let {
          it.repeat(HEADER_CHARACTER_LIMIT / it.length + 1)
        }

      beforeEach {
        every { event.throwableText } returns longStackTrace
      }

      it("trims the throwableText") {
        val urlSlot = slot<String>()
        every { GitLabUtil.browseUrl(capture(urlSlot)) } just runs

        errorReporter.submit(arrayOf(event), additionalInfo, parentComponent, consumer)

        urlSlot.captured.length shouldBeLessThan HEADER_CHARACTER_LIMIT
      }
    }
  }
})
