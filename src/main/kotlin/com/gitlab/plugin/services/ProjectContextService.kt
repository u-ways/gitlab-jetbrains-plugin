package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.pat.PatApi
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.codesuggestions.license.LicenseStatus
import com.gitlab.plugin.codesuggestions.telemetry.LogDestination
import com.gitlab.plugin.codesuggestions.telemetry.SnowplowDestination
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.util.gitlabStatusRefresh
import com.intellij.openapi.project.Project

class ProjectContextService(val project: Project) {
  private val patProvider by lazy { PatProvider() }

  private val notificationManager by lazy { GitLabNotificationManager() }

  private val exceptionHandler by lazy { CreateNotificationExceptionHandler(notificationManager) }

  private val duoContext: DuoContextService by lazy { DuoContextService.instance }

  val duoHttpClient by lazy {
    DuoClient(
      tokenProvider = patProvider,
      exceptionHandler = exceptionHandler,
      userAgent = duoContext.userAgent,
      host = duoContext.duoSettings.url,
      onStatusChanged = duoContext.iconStatusModifier,
      httpClientEngine = proxiedEngine()
    )
  }

  private val patApi by lazy {
    PatApi(token = patProvider.token(), host = duoContext.duoSettings.url)
  }

  val duoApi by lazy {
    DuoApi(
      client = duoHttpClient,
      telemetry = telemetry,
      duoSettings = duoContext.duoSettings,
      isLicensed = { codeSuggestionsLicense.isLicensed }
    )
  }

  private val serverService by lazy { GitLabServerService(duoApi) }

  // We don't want notifications or icon modifications when we check the version during project startup
  // These warnings are confusing to new users who have not set up their host/token yet
  val serverServiceWithoutCallbacks by lazy {
    GitLabServerService(
      DuoApi(
        duoHttpClient.copy(exceptionHandler = {}, onStatusChanged = object : DuoClient.DuoClientRequestListener {}),
        duoSettings = duoContext.duoSettings
      )
    )
  }

  val completionStrategy by lazy { CompletionStrategy(duoApi, serverService) }

  val telemetry by lazy {
    val snowplowTracker = GitLabApplicationService.getInstance().snowplowTracker
    Telemetry(
      listOf(LogDestination(), SnowplowDestination(snowplowTracker)),
      isEnabled = { duoContext.duoSettings.telemetryEnabled })
  }

  val codeSuggestionsLicense by lazy {
    LicenseStatus(
      patApi,
      DuoApi(
        duoHttpClient.copy(exceptionHandler = LicenseStatus.exceptionHandler),
        duoSettings = duoContext.duoSettings
      ),
      onLicenseChanged = ::gitlabStatusRefresh
    )
  }

  companion object {
    lateinit var instance: ProjectContextService

    fun init(project: Project): ProjectContextService {
      instance = ProjectContextService(project)
      return instance
    }
  }
}
