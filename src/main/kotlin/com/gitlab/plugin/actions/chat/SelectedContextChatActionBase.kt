package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.chat.extensions.fromEditor
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import kotlinx.coroutines.runBlocking

open class SelectedContextChatActionBase(
  private val content: String,
  private val recordType: ChatRecord.Type
) : AnAction() {
  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

  override fun actionPerformed(event: AnActionEvent) {
    val chatService = event.project?.service<ChatService>()
    val editor = event.getData(CommonDataKeys.EDITOR)

    if (
      chatService == null ||
      editor == null ||
      !editor.selectionModel.hasSelection()
    ) {
      return
    }

    runBlocking {
      chatService.processNewUserPrompt(
        NewUserPromptRequest(
          content,
          recordType,
          context = ChatRecordContext.fromEditor(editor)
        )
      )
    }
  }

  override fun update(event: AnActionEvent) {
    event.presentation.isVisible = BuildConfig.DUO_CHAT_ENABLED

    val caret = event.getData(CommonDataKeys.CARET)
    val hasSelectedText = caret?.hasSelection() ?: false

    event.presentation.isEnabled = hasSelectedText
  }
}
