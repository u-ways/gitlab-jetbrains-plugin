package com.gitlab.plugin.api.pat

import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.buildMockEngine
import com.gitlab.plugin.util.GitLabUtil
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.engine.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll

private const val PAT_INFO_RESPONSE =
  """{
    "id": 4,
    "name": "Test Token",
    "revoked": false,
    "created_at": "2020-07-23T14:31:47.729Z",
    "scopes": [
        "ai_features",
        "read_user"
    ],
    "user_id": 3,
    "last_used_at": "2021-10-06T17:58:37.550Z",
    "active": true,
    "expires_at": "2021-10-08T17:58:37.550Z"
  }"""

class PatApiTest : DescribeSpec({
  mockkObject(GitLabUtil)

  beforeEach {
    every { GitLabUtil.userAgent } returns "gitlab-plugin-test-agent"
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("patInfo") {
    context("when no host is provided") {
      it("uses GitLab.com as the default host") {
        val request = captureRequest {
          PatApi("", engine = it).patInfo()
        }

        request.url.host shouldBe "gitlab.com"
      }
    }

    context("when host is provided") {
      it("uses the provided host") {
        val request = captureRequest {
          PatApi("", host = "https://example.com", engine = it).patInfo()
        }

        request.url.host shouldBe "example.com"
      }
    }

    it("uses the plugin user agent") {
      val request = captureRequest {
        PatApi("", engine = it).patInfo()
      }

      request.headers["User-Agent"] shouldBe "gitlab-plugin-test-agent"
    }

    it("calls the PAT API") {
      val request = captureRequest {
        PatApi("", engine = it).patInfo()
      }

      request.url.encodedPath shouldBe "/api/v4/personal_access_tokens/self"
    }

    it("sends the provided PAT as a header") {
      val request = captureRequest {
        PatApi("test-pat", engine = it).patInfo()
      }

      request.headers["Authorization"] shouldBe "Bearer test-pat"
    }

    it("returns the PAT info") {
      PatApi("test-pat", engine = buildMockEngine(content = PAT_INFO_RESPONSE)).patInfo().let {
        it.id shouldBe 4
        it.name shouldBe "Test Token"
        it.revoked shouldBe false
        it.createdAt shouldBe "2020-07-23T14:31:47.729Z"
        it.scopes shouldBe listOf("ai_features", "read_user")
        it.userId shouldBe 3
        it.lastUsedAt shouldBe "2021-10-06T17:58:37.550Z"
        it.active shouldBe true
        it.expiresAt shouldBe "2021-10-08T17:58:37.550Z"
      }
    }

    context("when API returns 401") {
      it("throws GitLabUnauthorizedException") {
        shouldThrow<GitLabUnauthorizedException> {
          PatApi("test-pat", engine = buildMockEngine(statusCode = HttpStatusCode.Unauthorized)).patInfo()
        }
      }
    }
  }
})

private suspend fun captureRequest(block: suspend (HttpClientEngine) -> Unit): HttpRequestData {
  var requestData: HttpRequestData? = null

  val mockEngine = buildMockEngine(content = PAT_INFO_RESPONSE) { requestData = it }

  block(mockEngine)

  @Suppress("detekt:TooGenericExceptionThrown")
  return requestData ?: throw RuntimeException("Request data not found")
}
