package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.WithDataTestName
import io.kotest.datatest.withData
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*

class TelemetryTest : DescribeSpec({
  data class EventTestCase(val type: Event.Type, val run: (Telemetry) -> Unit) :
    WithDataTestName {
    override fun dataTestName() = type.toString()
  }

  val context =
    Event.Context(uuid = "uuid", modelEngine = "test-engine", modelName = "test-name", language = "test-lang")
  val destination1: Telemetry.Destination = mockk()
  val destination2: Telemetry.Destination = mockk()
  val telemetry = Telemetry(
    destinations = listOf(destination1, destination2),
    generateUUID = { "generated-uuid" },
    isEnabled = { true }
  )
  val dataTestCases = listOf(
    EventTestCase(Event.Type.ERROR) { it.error(context) },
    EventTestCase(Event.Type.REQUESTED) { it.requested(context) },
    EventTestCase(Event.Type.ACCEPTED) { it.accepted(context) },
    EventTestCase(Event.Type.LOADED) { it.loaded(context) },
    EventTestCase(Event.Type.CANCELLED) { it.cancelled(context) },
    EventTestCase(Event.Type.NOT_PROVIDED) { it.notProvided(context) },
    EventTestCase(Event.Type.SHOWN) { it.shown(context) },
    EventTestCase(Event.Type.REJECTED) { it.rejected(context) }
  )

  beforeEach {
    every { destination1.event(any()) } returns Unit
    every { destination2.event(any()) } returns Unit
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("dataTestCases") {
    it("contains a test case for every event type") {
      dataTestCases shouldHaveSize Event.Type.entries.size
    }
  }

  describe("newContext") {
    it("returns an event context with a generated UUID") {
      telemetry.newContext().uuid shouldBe "generated-uuid"
    }
  }

  context("when telemetry is enabled") {
    describe("broadcasts the event to all destinations") {
      withData(dataTestCases) { case ->
        every { destination1.event(any()) } returns Unit
        every { destination2.event(any()) } returns Unit

        case.run(telemetry)

        verifyAll {
          destination1.event(Event(case.type, context))
          destination2.event(Event(case.type, context))
        }

        clearAllMocks()
      }
    }

    describe("request") {
      it("stores the given context in currentContext") {
        val requestedContext = context.copy(prefixLength = 5, suffixLength = 6)

        telemetry.requested(requestedContext)

        telemetry.currentContext shouldBe requestedContext
      }
    }

    describe("load") {
      it("stores the given context in currentContext") {
        val loadedContext =
          context.copy(modelName = "loaded-name", modelEngine = "loaded-engine", language = "loaded-lang")

        telemetry.loaded(loadedContext)

        telemetry.currentContext shouldBe loadedContext
      }
    }
  }

  context("when telemetry is disabled") {
    val disabledTelemetry = Telemetry(destinations = listOf(destination1, destination2), isEnabled = { false })

    describe("does not broadcast the event") {
      withData(dataTestCases) { case ->
        case.run(disabledTelemetry)

        verifyAll(inverse = true) {
          destination1.event(any())
          destination2.event(any())
        }
      }
    }
  }
})
