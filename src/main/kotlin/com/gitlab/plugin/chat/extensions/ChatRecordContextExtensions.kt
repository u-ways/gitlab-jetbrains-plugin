package com.gitlab.plugin.chat.extensions

import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.intellij.openapi.editor.Editor

fun ChatRecordContext.Companion.fromEditor(editor: Editor): ChatRecordContext? {
  val project = editor.project
  val virtualFile = editor.virtualFile
  val document = editor.document
  val caret = editor.caretModel.primaryCaret

  val documentLength = document.charsSequence.length
  val indexOutOfBound = caret.selectionStart > documentLength || caret.selectionEnd > documentLength

  if (indexOutOfBound || project == null || virtualFile == null) return null

  val fileName = virtualFile.path.removePrefix(project.basePath.orEmpty())
  val selectedText = caret.selectedText.orEmpty()
  val contentAboveCursor = document.charsSequence.take(caret.selectionStart).toString()
  val contentBelowCursor = document.charsSequence.drop(caret.selectionEnd).toString()

  val fileContext = ChatRecordFileContext(
    fileName = fileName,
    selectedText = selectedText,
    contentAboveCursor = contentAboveCursor,
    contentBelowCursor = contentBelowCursor
  )

  return ChatRecordContext(fileContext)
}
