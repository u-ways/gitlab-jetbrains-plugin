package com.gitlab.plugin.chat.exceptions

class UnsupportedGitLabVersionException(version: String) :
  GraphQLException("GitLab $version does not support this operation.")
