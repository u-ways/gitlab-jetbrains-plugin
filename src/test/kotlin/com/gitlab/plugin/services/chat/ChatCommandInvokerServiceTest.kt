package com.gitlab.plugin.services.chat

import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.DataContext
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class ChatCommandInvokerServiceTest : DescribeSpec({
  beforeContainer {
    mockkStatic(ActionManager::class)
    mockkStatic(DataManager::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("ChatCommandInvokerService") {
    val actionManager = mockk<ActionManager>()
    val dataManager = mockk<DataManager>()

    beforeEach {
      clearMocks(actionManager, dataManager)
      every { ActionManager.getInstance() } returns actionManager
      every { DataManager.getInstance() } returns dataManager
    }

    describe("invokeCommand") {
      it("calls the correct action for a known command") {
        // arrange
        val knownCommand = "/explain"
        val mockAction = mockk<AnAction>(relaxed = true)
        val mockDataContext = mockk<DataContext>(relaxed = true)
        every { actionManager.getAction(any()) } returns mockAction

        every { dataManager.dataContextFromFocusAsync.onSuccess(any()) } answers {
          firstArg<java.util.function.Consumer<DataContext>>().accept(mockDataContext)
          this.callOriginal()
        }

        val service = ChatCommandInvokerService()

        // act
        service.invokeCommand(knownCommand)

        // assert
        verify { mockAction.actionPerformed(any()) }
      }
    }

    describe("isKnownCommand") {
      val service = ChatCommandInvokerService()

      it("returns true for a known command") {
        // Assert
        service.isKnownCommand("/explain") shouldBe true
      }

      it("returns false for an unknown command") {
        // Assert
        service.isKnownCommand("/unknown") shouldBe false
      }
    }
  }
})
