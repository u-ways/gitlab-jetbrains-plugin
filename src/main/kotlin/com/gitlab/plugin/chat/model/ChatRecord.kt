package com.gitlab.plugin.chat.model

import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class ChatRecordFileContext(
  val fileName: String,
  val selectedText: String,
  val contentAboveCursor: String? = null,
  val contentBelowCursor: String? = null
)

@Serializable
data class ChatRecordContext(
  val currentFile: ChatRecordFileContext
)

@Serializable
data class ChatRecord(
  val id: String = UUID.randomUUID().toString(),
  val role: Role,
  val type: Type = Type.GENERAL,
  val requestId: String,
  var state: State,
  var content: String? = null,
  var contentHtml: String? = null,
  var timestamp: LocalDateTime = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
  val extras: Extras? = null,
  val context: ChatRecordContext? = null
) {
  val errors: MutableList<String> = mutableListOf()
  val chunks: MutableList<String> = mutableListOf()

  companion object {
    fun pendingAssistantResponse(userChatRecord: ChatRecord): ChatRecord {
      require(userChatRecord.role == Role.USER) { "Assistant chat records can only be created from user chat records." }

      return ChatRecord(
        requestId = userChatRecord.requestId,
        role = Role.ASSISTANT,
        type = userChatRecord.type,
        state = State.PENDING
      )
    }
  }

  @Serializable
  data class Extras(val sources: List<Source>)

  @Serializable
  data class Source(val name: String, val url: String)

  @Serializable
  enum class State {
    @SerialName("pending")
    PENDING,

    @SerialName("ready")
    READY
  }

  @Serializable
  enum class Role {
    @SerialName("user")
    USER,

    @SerialName("assistant")
    ASSISTANT,

    @SerialName("system")
    SYSTEM;

    companion object {
      fun fromValue(value: String): Role = when (value) {
        "user" -> USER
        "assistant" -> ASSISTANT
        "system" -> SYSTEM
        else -> throw IllegalArgumentException("unknown value: $value")
      }
    }
  }

  @Serializable
  enum class Type {
    @SerialName("general")
    GENERAL,

    @SerialName("explainCode")
    EXPLAIN_CODE,

    @SerialName("generateTests")
    GENERATE_TESTS,

    @SerialName("refactorCode")
    REFACTOR_CODE,

    @SerialName("newConversation")
    NEW_CONVERSATION
  }
}
