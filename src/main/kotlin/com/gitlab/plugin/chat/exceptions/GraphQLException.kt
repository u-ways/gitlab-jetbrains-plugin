package com.gitlab.plugin.chat.exceptions

open class GraphQLException(override val message: String) : RuntimeException()
