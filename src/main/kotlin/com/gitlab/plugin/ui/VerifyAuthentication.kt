package com.gitlab.plugin.ui

import com.gitlab.plugin.api.GitLabForbiddenException
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabProxyException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.ProjectContextService
import com.intellij.openapi.diagnostic.Logger
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import kotlinx.coroutines.runBlocking
import javax.swing.JLabel

fun verifyServerConfiguration(
  host: String,
  token: String,
  updatableLabel: JLabel,
  errorReportingInstructions: Cell<BrowserLink>
) = runBlocking {
  errorReportingInstructions.visible(false)
  updatableLabel.text = "Verifying..."

  var errorMessage = ""
  val client = ProjectContextService.instance.duoHttpClient.copy(
    host = host,
    tokenProvider = { token },
    shouldRetry = false,
    exceptionHandler = { exception ->
      errorMessage = when (exception) {
        is GitLabOfflineException -> "Server cannot be reached."
        is GitLabForbiddenException -> "Personal Access Token is insufficient."
        is GitLabUnauthorizedException ->
          "Personal Access Token is invalid or Code Suggestions is not available on your instance"

        is GitLabProxyException -> "Proxy is not setup correctly."
        else -> {
          Logger.getInstance(this::class.java).error(exception)
          errorReportingInstructions.visible(true)
          "An unexpected error occurred. See the error notification for more details, or click the link below to learn how to report this error."
        }
      }
    }
  )

  val verified = DuoApi(client, duoSettings = DuoPersistentSettings()).verify()

  if (verified) {
    updatableLabel.text = "Success! The server host and Personal Access Token are correctly setup"
  } else {
    val errorMessageHTML = "<html><body style='width: 350px;'>%s</body></html>"
    updatableLabel.text = String.format(errorMessageHTML, errorMessage)
  }
}
