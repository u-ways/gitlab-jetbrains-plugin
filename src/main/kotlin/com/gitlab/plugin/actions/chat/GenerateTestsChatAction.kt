package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.chat.model.ChatRecord

/**
 * Action will generate tests based on currently selected code with GitLab Duo Chat
 */
class GenerateTestsChatAction : SelectedContextChatActionBase(
  GENERATE_TESTS_CHAT_CONTENT,
  ChatRecord.Type.GENERATE_TESTS
) {
  companion object {
    const val GENERATE_TESTS_CHAT_CONTENT = "/tests"
  }
}
