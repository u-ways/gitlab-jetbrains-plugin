package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.api.duo.graphql.Chat
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.graphql.CurrentUserQuery

class GraphQLApi(apolloClientFactory: ApolloClientFactory) {
  private val apolloClient: ApolloClient by lazy { apolloClientFactory.create() }

  @Suppress("detekt:VariableNaming", "PropertyName")
  val Chat: Chat
    get() = Chat(apolloClient)

  @Throws(GitLabGraphQLResponseException::class)
  suspend fun getCurrentUser() = try {
    apolloClient.query(CurrentUserQuery())
      .execute()
      .dataAssertNoErrors
      .currentUser
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }
}
