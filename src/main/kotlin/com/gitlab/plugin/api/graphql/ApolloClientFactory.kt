package com.gitlab.plugin.api.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.okHttpClient
import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.util.GitLabUtil.GITLAB_DEFAULT_URL
import com.intellij.collaboration.util.resolveRelative
import com.intellij.util.net.HttpConfigurable
import io.ktor.http.HttpHeaders.Authorization
import okhttp3.OkHttpClient
import java.net.URI

class ApolloClientFactory(
  private val tokenProvider: DuoClient.TokenProvider,
  gitlabHost: String = GITLAB_DEFAULT_URL,
) {
  private val serverUrl: String = URI(gitlabHost)
    .resolveRelative("/api/graphql")
    .toString()
  private val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder().apply {
      followRedirects(true)

      val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
      proxyInfo?.let {
        ProxyManager(it) { url -> HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(url) }.let { proxyManager ->
          proxySelector(proxyManager)
          proxyAuthenticator(proxyManager)
        }
      }
    }.build()
  }

  fun create(): ApolloClient = ApolloClient.Builder()
    .okHttpClient(okHttpClient)
    .serverUrl(serverUrl)
    .addHttpHeader(Authorization, "Bearer ${tokenProvider.token()}")
    .build()
}
