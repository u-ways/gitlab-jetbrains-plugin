package com.gitlab.plugin.chat.view.abstraction

import com.intellij.openapi.Disposable

interface ChatBrowser : Disposable {
  fun postMessage(payload: String)
  fun onPostMessageReceived(block: (String) -> Unit)
  fun addLocalResource(path: String, resourcePathPrefix: String, mimeType: String)
}
