package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.util.TextRange
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class DisallowedCharactersPastCursorTest : DescribeSpec({
  val logger: Logger = mockk()
  val request: InlineCompletionRequest = mockk()
  val disallowedCharactersPastCursor = DisallowedCharactersPastCursor(logger)

  afterEach {
    clearMocks(logger, request)
  }

  context("when cursor is at end of line") {
    beforeEach {
      val currentPosition = 10
      val lineNumber = 1

      every { request.endOffset } returns currentPosition
      every { request.document.getLineNumber(currentPosition) } returns lineNumber
      every { request.document.getLineEndOffset(lineNumber) } returns currentPosition
    }

    it("returns false") {
      disallowedCharactersPastCursor.shouldSkipSuggestion(request) shouldBe false
    }

    it("does not log a message") {
      disallowedCharactersPastCursor.shouldSkipSuggestion(request)

      verify(exactly = 0) { logger.info(any<String>()) }
    }
  }

  context("when only special characters are after the cursor on the current line") {
    val cases = listOf(
      ")", "}", "]", "'", "\"", "\'", "`", ":", "{", ";", ",", " )", ")}", ");", ") ", " ) ", " )"
    )

    withData(cases) { charsAfterCursor ->
      setupRequestMock(
        request = request,
        currentPosition = 5,
        lineNumber = 1,
        charsAfterCursor = charsAfterCursor
      )

      withClue("returns false for $charsAfterCursor") {
        disallowedCharactersPastCursor.shouldSkipSuggestion(request) shouldBe false
      }

      withClue("does not log a message") {
        verify(exactly = 0) { logger.info(any<String>()) }
      }

      clearMocks(logger, request)
    }
  }

  context("when non-special characters are after the cursor on the current line") {
    withData(listOf("text", " text", " text ", "with(", ": Int", "print()")) { charsAfterCursor ->
      setupRequestMock(
        request = request,
        currentPosition = 5,
        lineNumber = 1,
        charsAfterCursor = charsAfterCursor
      )
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      withClue("returns true for $charsAfterCursor") {
        disallowedCharactersPastCursor.shouldSkipSuggestion(request) shouldBe true
      }

      withClue("logs a message") {
        verify(exactly = 1) { logger.info(any<String>()) }
        logMessageSlot.captured shouldContain "disallowed characters past cursor"
      }

      clearMocks(logger, request)
    }
  }
})

private fun setupRequestMock(
  request: InlineCompletionRequest,
  currentPosition: Int,
  lineNumber: Int,
  charsAfterCursor: String
) {
  val lineEndOffset = currentPosition + charsAfterCursor.length

  every { request.endOffset } returns currentPosition
  every { request.document.getLineNumber(currentPosition) } returns lineNumber
  every { request.document.getLineEndOffset(lineNumber) } returns lineEndOffset
  every { request.document.getText(TextRange(currentPosition, lineEndOffset)) } returns charsAfterCursor
}
