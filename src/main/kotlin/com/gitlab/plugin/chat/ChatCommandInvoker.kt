package com.gitlab.plugin.chat

interface ChatCommandInvoker {
  fun invokeCommand(command: String)
  fun isKnownCommand(command: String): Boolean
}
