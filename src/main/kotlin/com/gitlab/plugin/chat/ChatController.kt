package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.exceptions.ChatException
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.*
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.LocalDateTime
import java.util.*

class ChatController(
  private val chatApiClient: ChatApiClient,
  private val chatView: ChatView,
  private val chatCommandInvoker: ChatCommandInvoker,
  private val chatHistory: ChatHistory = ChatHistory(),
  private val logger: Logger = logger<ChatController>()
) {
  init {
    chatView.onMessage { message ->
      runBlocking {
        handleViewOnMessage(message)
      }
    }
  }

  fun showChatWindow() = chatView.show()

  suspend fun processNewUserPrompt(prompt: NewUserPromptRequest) {
    val subscriptionId = UUID.randomUUID().toString()

    val actionResponse = chatApiClient.processNewUserPrompt(subscriptionId = subscriptionId, question = prompt.content)
    if (actionResponse.aiAction.errors.isNotEmpty()) {
      throw ChatException("Error processing new user prompt: ${actionResponse.aiAction.errors.joinToString(", ")}")
    }

    val record = ChatRecord(
      id = subscriptionId,
      role = ChatRecord.Role.USER,
      type = prompt.type,
      state = ChatRecord.State.READY,
      requestId = actionResponse.aiAction.requestId,
      context = prompt.context,
      content = prompt.content,
      contentHtml = null,
      extras = null
    )

    record.addToChat()
    showChatWindow()

    // if the chat starts a new conversation, we don't need an assistant response
    if (record.type != ChatRecord.Type.NEW_CONVERSATION) {
      ChatRecord.pendingAssistantResponse(record).let { assistantChatRecord ->
        assistantChatRecord.addToChat()
        chatApiClient.subscribeToUpdates(subscriptionId, ::handleAiMessageUpdate)
      }
    }
  }

  private suspend fun handleViewOnMessage(message: ChatViewMessage) = when (message) {
    is AppReadyMessage -> {
      chatHistory.records.forEach { chatView.addRecord(NewRecordMessage(it)) }
    }
    is NewPromptMessage -> handleNewPromptMessage(message)
    else -> logger.warn("Unhandled chat-webview message: $message")
  }

  private suspend fun handleNewPromptMessage(
    message: NewPromptMessage
  ) = when {
    chatCommandInvoker.isKnownCommand(message.content) -> chatCommandInvoker.invokeCommand(message.content)
    else -> processNewUserPrompt(NewUserPromptRequest(message.content))
  }

  private fun handleAiMessageUpdate(message: AiMessage) {
    val record = chatHistory.findViaRequestId(message.requestId, ChatRecord.Role.fromValue(message.role))
    if (record == null) {
      logger.error("No record found for requestId: ${message.requestId}")
      return
    }

    // the presence of 'chunkId' indicates a streaming response
    if (message.chunkId != null) {
      // for streaming responses, update the specific chunk in the record
      record.chunks[message.chunkId - 1] = message.content
    } else {
      // for non-streaming (complete) responses, update the record with the full content received
      record.content = message.content
      record.contentHtml = message.contentHtml
      record.errors.addAll(message.errors)

      val timestamp = runCatching { LocalDateTime.parse(message.timestamp) }.getOrNull()
      if (timestamp != null) record.timestamp = timestamp
    }

    record.state = ChatRecord.State.READY
    chatView.updateRecord(UpdateRecordMessage(record))
  }

  private fun ChatRecord.addToChat() {
    chatHistory.addRecord(this)
    chatView.addRecord(NewRecordMessage(this))
  }
}
