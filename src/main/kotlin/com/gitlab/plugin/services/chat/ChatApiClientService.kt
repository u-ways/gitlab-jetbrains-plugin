package com.gitlab.plugin.services.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import java.util.*

private const val AI_RESPONSE_DELAY = 1000L

@Suppress("detekt:all") // This is a test implementation, don't check code quality
internal class ChatApiClientService : ChatApiClient {
  val requestIdsBySubscriptionIds = HashMap<String, String>()

  override suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String
  ): AiActionResponse {
    val requestId = UUID.randomUUID().toString()
    requestIdsBySubscriptionIds[subscriptionId] = requestId

    return AiActionResponse(AiAction(requestId, listOf()))
  }

  override suspend fun subscribeToUpdates(
    subscriptionId: String,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  ) {
    val requestId = requestIdsBySubscriptionIds[subscriptionId]
      ?: throw Exception("Could not find requestId")

    CoroutineScope(Dispatchers.Default).launch {
      delay(AI_RESPONSE_DELAY)

      val content = "[AI Response]"
      val response =
        AiMessage(
          requestId = requestId,
          role = "assistant",
          content = content,
          contentHtml = "",
          timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString()
        )

      onMessageReceived(response)
    }

    return
  }
}
