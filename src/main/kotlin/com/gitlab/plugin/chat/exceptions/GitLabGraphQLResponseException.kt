package com.gitlab.plugin.chat.exceptions

class GitLabGraphQLResponseException(cause: Throwable?) :
  GraphQLException(cause?.message ?: "unexpected error")
