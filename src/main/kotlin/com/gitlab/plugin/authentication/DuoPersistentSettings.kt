package com.gitlab.plugin.authentication

import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.util.application


@State(name = "GitLabAccount", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class DuoPersistentSettings : PersistentStateComponent<DuoPersistentSettings.State> {
  data class State(
    var url: String = GitLabUtil.GITLAB_DEFAULT_URL,
    var enabled: Boolean = true,
    var telemetryEnabled: Boolean = true,
  )

  private var state: State = State()

  override fun getState(): State = state

  override fun loadState(state: State) {
    this.state = state
  }

  fun toggleEnabled(): Boolean {
    enabled = !enabled

    return enabled
  }

  var url
    get() = state.url
    set(value) {
      state.url = value
    }

  var enabled
    get() = state.enabled
    set(value) {
      state.enabled = value
    }

  var telemetryEnabled
    get() = state.telemetryEnabled
    set(value) {
      state.telemetryEnabled = value
    }

  fun gitlabRealm() = if (url.matches(GITLAB_COM_REGEX)) GITLAB_REALM_SAAS else GITLAB_REALM_SELF_MANAGED

  companion object {
    const val GITLAB_REALM_SAAS = "saas"
    const val GITLAB_REALM_SELF_MANAGED = "self-managed"
    private val GITLAB_COM_REGEX = Regex("https?://gitlab\\.com")

    fun getInstance(): DuoPersistentSettings = application.service()
  }
}
