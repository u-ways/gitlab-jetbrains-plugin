package com.gitlab.plugin.codesuggestions.license

import com.gitlab.plugin.api.GitLabResponseException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.pat.PatApi
import com.gitlab.plugin.api.pat.PatInfo
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.plugins.*
import io.ktor.http.*
import io.mockk.*

class LicenseStatusTest : DescribeSpec({
  val duoApi: DuoApi = mockk()
  val patApi: PatApi = mockk()
  val onLicenseChanged: () -> Unit = mockk()
  lateinit var licenseStatus: LicenseStatus

  beforeEach {
    licenseStatus = LicenseStatus(patApi, duoApi, onLicenseChanged)
    coEvery { duoApi.verify() } returns true
    coEvery { patApi.patInfo() } returns buildPatInfo()
    every { onLicenseChanged() } just runs
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("licensed") {
    it("defaults to true") {
      licenseStatus.isLicensed shouldBe true
    }
  }

  describe("refresh") {
    context("when PAT is not active") {
      beforeEach {
        coEvery { patApi.patInfo() } returns buildPatInfo(active = false)
      }

      it("does not call onLicenseChanged") {
        licenseStatus.refresh()

        verify(exactly = 0) { onLicenseChanged() }
      }
    }

    context("when PAT does not have ai_features scope") {
      beforeEach {
        coEvery { patApi.patInfo() } returns buildPatInfo(scopes = listOf("other"))
      }

      it("does not call onLicenseChanged") {
        licenseStatus.refresh()

        verify(exactly = 0) { onLicenseChanged() }
      }
    }

    context("when PAT API throws ResponseException") {
      beforeEach {
        coEvery { patApi.patInfo() } throws mockk<ResponseException>(relaxed = true)
      }

      it("does not call onLicenseChanged") {
        licenseStatus.refresh()

        verify(exactly = 0) { onLicenseChanged() }
      }
    }

    context("when Duo verification request returns true") {
      beforeEach {
        licenseStatus.isLicensed = false
        coEvery { duoApi.verify() } returns true
      }

      it("sets licensed to true and calls onLicenseChanged") {
        licenseStatus.refresh()

        licenseStatus.isLicensed shouldBe true
        verify(exactly = 1) { onLicenseChanged() }
      }
    }

    context("when Duo verification request returns false") {
      beforeEach {
        licenseStatus.isLicensed = true
        coEvery { duoApi.verify() } returns false
      }

      it("does not call onLicenseChanged") {
        licenseStatus.refresh()

        verify(exactly = 0) { onLicenseChanged() }
      }
    }

    context("when Duo verification request throws GitLabResponseException") {
      context("with status code 404") {
        val exception = mockk<GitLabResponseException>(relaxed = true) {
          every { response.status } returns HttpStatusCode.NotFound
        }

        beforeEach {
          licenseStatus.isLicensed = true
          coEvery { duoApi.verify() } throws exception
        }

        it("sets licensed to false and calls onLicenseChanged") {
          licenseStatus.refresh()

          licenseStatus.isLicensed shouldBe false
          verify(exactly = 1) { onLicenseChanged() }
        }
      }

      context("with other status code") {
        val exception = mockk<GitLabResponseException>(relaxed = true) {
          every { response.status } returns HttpStatusCode.Forbidden
        }

        beforeEach {
          licenseStatus.isLicensed = true
          coEvery { duoApi.verify() } throws exception
        }

        it("does not call onLicenseChanged") {
          licenseStatus.refresh()

          verify(exactly = 0) { onLicenseChanged() }
        }
      }
    }

    context("when Duo verification request throws GitLabUnauthorizedException") {
      val exception = mockk<GitLabUnauthorizedException>(relaxed = true)

      beforeEach {
        licenseStatus.isLicensed = true
        coEvery { duoApi.verify() } throws exception
      }

      it("sets licensed to false and calls onLicenseChanged") {
        licenseStatus.refresh()

        licenseStatus.isLicensed shouldBe false
        verify(exactly = 1) { onLicenseChanged() }
      }
    }
  }
})

private fun buildPatInfo(active: Boolean = true, scopes: List<String> = listOf(PatInfo.AI_FEATURES_SCOPE)) = PatInfo(
  id = 1,
  name = "test token",
  revoked = false,
  createdAt = "2022-01-01T00:00:00Z",
  scopes = scopes,
  userId = 1,
  lastUsedAt = "2022-01-02T00:00:00Z",
  active = active,
  expiresAt = "2022-01-03T00:00:00Z"
)
