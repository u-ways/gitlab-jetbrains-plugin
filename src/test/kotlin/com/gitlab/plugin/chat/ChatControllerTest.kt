package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.AppReadyMessage
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewPromptMessage
import com.intellij.openapi.diagnostic.Logger
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.runBlocking
import java.util.*

class ChatControllerTest : DescribeSpec({
  lateinit var controller: ChatController
  val api = mockk<ChatApiClient>(relaxed = true)
  val view = mockk<ChatView>(relaxed = true)
  val chatCommandInvoker = mockk<ChatCommandInvoker>(relaxed = true)
  val logger = mockk<Logger>(relaxed = true)
  lateinit var chatHistory: ChatHistory
  lateinit var viewOnMessageCallback: ((message: ChatViewMessage) -> Unit)

  beforeEach {
    every {
      view.onMessage(captureLambda())
    } answers {
      viewOnMessageCallback = lambda<(message: ChatViewMessage) -> Unit>().captured
    }

    chatHistory = ChatHistory()
    controller =
      ChatController(api, view, chatHistory = chatHistory, chatCommandInvoker = chatCommandInvoker, logger = logger)

    every { chatCommandInvoker.isKnownCommand(any()) } returns false
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("chatView") {
      describe("onMessage") {
        describe("NewPromptMessage") {
          it("invokes known command") {
            // arrange
            val knownCommand = "/knownCommand"
            every { chatCommandInvoker.isKnownCommand(knownCommand) } returns true

            // act
            runBlocking { viewOnMessageCallback(NewPromptMessage(knownCommand)) }

            // assert
            coVerify { chatCommandInvoker.invokeCommand(knownCommand) }
            verify(exactly = 0) { view.show() }
          }
          it("processes new user prompt") {
            // act
            val prompt = "ping"
            viewOnMessageCallback.invoke(NewPromptMessage(prompt))

            // assert
            verify(exactly = 1) { view.show() }
            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[0]
                }
              )
            }

            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[1]
                }
              )
            }
          }
        }
        describe("appReadyMessage") {
          it("restores history to view") {
            // arrange
            val chatRecords = listOf(
              ChatRecord(
                role = ChatRecord.Role.USER,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "User message"
              ),
              ChatRecord(
                role = ChatRecord.Role.ASSISTANT,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "Assistant response"
              )
            )

            chatRecords.forEach { chatHistory.addRecord(it) }

            // act
            viewOnMessageCallback.invoke(AppReadyMessage)

            // assert
            // Verify each record in chat history is added to the view
            chatRecords.forEach { record ->
              verify {
                view.addRecord(
                  match {
                    it.record == record
                  }
                )
              }
            }
          }
        }
      }
    }
    describe("processNewUserPrompt") {
      describe("with GENERAL type and valid input") {
        it("add user and assistant record") {
          // arrange
          val content = "ping"
          val requestId = "00000000-00000000-00000000-00000000"

          coEvery {
            api.processNewUserPrompt(any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          controller.processNewUserPrompt(NewUserPromptRequest(content))

          // assert

          // assert chat history
          chatHistory.records shouldHaveSize 2

          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.content shouldBe content
          }

          chatHistory.records[1].role shouldBe ChatRecord.Role.ASSISTANT

          val subscriptionId = chatHistory.records[0].id

          // assert view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[1]
              }
            )
          }

          // assert api client
          coVerify { api.processNewUserPrompt(subscriptionId, content) }
          coVerify { api.subscribeToUpdates(subscriptionId, any()) }
        }
        it("sets up subscription and handles updates correctly") {
          // arrange
          val content = "ping"
          val requestId = UUID.randomUUID().toString()
          val aiActionResponse = AiActionResponse(AiAction(requestId, emptyList()))
          val newUserPromptRequest = NewUserPromptRequest(content)

          coEvery { api.processNewUserPrompt(any(), any()) } returns aiActionResponse

          val assistantContent = "pong"
          val aiMessage =
            AiMessage(
              requestId,
              role = "assistant",
              content = assistantContent,
              contentHtml = "",
              timestamp = ""
            )

          coEvery { api.subscribeToUpdates(any(), any()) } coAnswers {
            secondArg<suspend (message: AiMessage) -> Unit>().invoke(aiMessage)
          }

          // act
          controller.processNewUserPrompt(newUserPromptRequest)

          // assert
          // Verify that subscribeToUpdates is called with correct parameters
          coVerify { api.subscribeToUpdates(any(), any()) }

          // Verify that the chat history and view are updated with the new information from the subscription
          chatHistory.records.last().let {
            it.role shouldBe ChatRecord.Role.ASSISTANT
            it.content shouldBe assistantContent

            // Verify that the view is updated with the new assistant record
            verify {
              view.updateRecord(
                match { message ->
                  message.record == it
                }
              )
            }
          }
        }
      }
      describe("with NEW_CONVERSATION type") {
        it("adds user record while not adding new assistant record") {
          // arrange
          val content = "Start New Conversation"
          val requestId = "newConvRequestId"
          val newUserPromptRequest = NewUserPromptRequest(content, ChatRecord.Type.NEW_CONVERSATION)

          coEvery {
            api.processNewUserPrompt(any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          controller.processNewUserPrompt(newUserPromptRequest)

          // assert

          // Assert that a new conversation record is added to chat history
          chatHistory.records shouldHaveSize 1
          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.type shouldBe ChatRecord.Type.NEW_CONVERSATION
            it.content shouldBe content
          }

          // Assert that the view is shown and the record is added to the view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          // Assert that no assistant record is created and no subscription to updates is made
          coVerify(exactly = 0) { api.subscribeToUpdates(any(), any()) }
        }
      }
    }
  }
})
